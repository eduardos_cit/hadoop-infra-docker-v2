DOCKER_NETWORK = docker-hadoop_default
ENV_FILE = hadoop.env
current_version = 0.0.1
build:
	docker build -t eduardoscit/base:$(current_version) ./images/base
	docker build -t eduardoscit/hadoop-base:$(current_version) ./images/hadoop/base
	docker build -t eduardoscit/hadoop-datanode:$(current_version) ./images/hadoop/datanode
	docker build -t eduardoscit/hadoop-historyserver:$(current_version) ./images/hadoop/historyserver
	docker build -t eduardoscit/hadoop-namenode:$(current_version) ./images/hadoop/namenode
	docker build -t eduardoscit/hadoop-nodemanager:$(current_version) ./images/hadoop/nodemanager
	docker build -t eduardoscit/hadoop-resourcemanager:$(current_version) ./images/hadoop/resourcemanager
	
	docker build -t eduardoscit/hive-base:$(current_version) ./images/hive/base
	docker build -t eduardoscit/hive-server:$(current_version) ./images/hive/hive-server
	docker build -t eduardoscit/hive-metastore:$(current_version) ./images/hive/metastore
	docker build -t eduardoscit/hive-postgres:$(current_version) ./images/postgres
	
	docker build -t eduardoscit/spark-base:$(current_version)   ./images/spark/base
	docker build -t eduardoscit/spark-master:$(current_version) ./images/spark/master
	docker build -t eduardoscit/spark-worker:$(current_version) ./images/spark/worker
push:
	docker push eduardoscit/base:$(current_version)
	docker push eduardoscit/hadoop-base:$(current_version)
	docker push eduardoscit/hadoop-datanode:$(current_version)
	docker push eduardoscit/hadoop-historyserver:$(current_version)
	docker push eduardoscit/hadoop-namenode:$(current_version)
	docker push eduardoscit/hadoop-nodemanager:$(current_version)
	docker push eduardoscit/hadoop-resourcemanager:$(current_version)
	
	docker push eduardoscit/hive-base:$(current_version)
	docker push eduardoscit/hive-server:$(current_version)
	docker push eduardoscit/hive-metastore:$(current_version)
	docker push eduardoscit/hive-postgres:$(current_version)
	
	docker push eduardoscit/spark-base:$(current_version)
	docker push eduardoscit/spark-master:$(current_version)
	docker push eduardoscit/spark-worker:$(current_version)